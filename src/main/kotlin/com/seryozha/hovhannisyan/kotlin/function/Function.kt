package com.seryozha.hovhannisyan.kotlin.function

// a file without class


fun main() {
    printMessage("Hello")
    //type  mismatch
//    printMessageWithPrefix("Hello", "Log")
    printMessageWithPrefix("Hello", 5,"Log")
    printMessageWithPrefix("Hello")
    printMessageWithPrefix(prefix = "Log", message = "Hello")
    println(sum(1, 2))
}

/**
 * The type with only one value: the `Unit` object. This type corresponds to the `void` type in Java.
 */
fun printMessage(message: String): Unit {
    println(message)
}

fun printMessageWithPrefix(message: String,
                           prefixInt: Int = 0,
                           prefix: String = "Info") {
    println("[$prefix] $message prefixInt=[$prefixInt]")
}

fun sum(x: Int, y: Int): Int {
    return x + y
}

fun multiply(x: Int, y: Int) = x * y

