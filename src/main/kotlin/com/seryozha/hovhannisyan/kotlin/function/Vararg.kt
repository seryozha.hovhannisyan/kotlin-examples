package com.seryozha.hovhannisyan.kotlin.function

fun main() {

    fun printAll(vararg messages: String) {                            // 1
        for (m in messages) println(m)
    }

    printAll("Hello", "Hallo", "Salut", "Hola", "你好")                 // 2

    fun printAllWithPrefix(vararg messages: String, prefix: String) {  // 3
        for (m in messages) println(prefix + m)
    }
    printAllWithPrefix(
            "Hello", "Hallo", "Salut", "Hola", "你好",
            prefix = "Greeting: "                                          // 4
    )

    fun log(vararg entries: String) {
        printAll(*entries)                                             // 5
    }

    fun a1 () {
        fun a2 () {
            fun a2 () {
                fun a2 () {
                    fun a2 () {
                        fun a2 () {
                            fun a2 () {
                                fun a2 () {
                                    fun a2 () {
                                        fun a2 () {
                                            println("11")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        println("1")

    }


    a1();
}